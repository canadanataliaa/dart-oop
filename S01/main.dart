// The keyword "void" means function will not return anything
// Syntax of a function: <return type> functionName(parameters) {}
void main() {
  String firstName = 'John';
  String? middleName = null; // by adding "?", the variable can accept a null
  String lastName = 'Smith';
  int age = 31; // numbers without decimal points
  double height = 172.45; // numbers with decimal points
  num weight = 64.32; // numbers with or without decimal points
  bool isRegistered = false;
  List<num> grades = [98.2, 89, 87.88, 91.2]; // arrays are called lists in Dart

  // Map is an object, key-value pair should be done in a json like format
  // In Dart, key and value can declare its data type: Map<keyDatatype, valueDatatype>
  // Map<String, dynamic> person = {    ..but can also be omitted
  Map<String, dynamic> personA = {'name': 'Brandon', 'batch': 213};

  // Another method to create an object (Map)
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

  // "final" variable can only be set once (run-time constants)
  // "final" variables can be given a value later on Ex. final DateTime now;
  final DateTime now = DateTime.now();
  // now = DateTime.now();

  // "const" should have a value when declared (compile-time constants)
  // "const" cannot be assigned a value after declaring
  // With const, an identier must have a corresponding declaration of value (identifier == companyAcronym)
  const String companyAcronym = 'FFUF';
  // companyAcronym = 'FFUF';

  // print(firstName + ' ' + lastName); // String concatenation
  print('Full name: $firstName $lastName');
  print('Age: ' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('Registered: ' + isRegistered.toString());
  print('Grades: ' + grades.toString());
  print('Current Datetime: ' + now.toString());
  print(personB['name']);
}
