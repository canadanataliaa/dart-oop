// class _Worker {
//   String _getType() {
//     return 'Worker';
//   }
// }

// class Carpenter extends _Worker {
//   String getType() {
//     return super._getType();
//   }
// }

abstract class _Worker {
  String getType();
}

class Carpenter implements _Worker {
  String getType() {
    return 'Carpenter';
  }
} 