import 'worker.dart';

void main() {
  // Person personA = new Person();
  Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
  // Worker worker = new Worker();
  Carpenter carpenter = new Carpenter();

  print(carpenter.getType());
}

// An abstract class cannot instatiate an object
// It has to have a concrete subclass
abstract class Person {
  // The class Person defines that a getFullNaem must be implemented
  // But it does not tell HOW to implement it.
  // Abstract methods does not have a block of code, no implementation
  String getFullName();   
}

// Doctor class (concrete class) implements abstract concepts from Person
class Doctor implements Person {  
  String firstName;
  String lastName;

  Doctor({
    required this.firstName,
    required this.lastName
  });

  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

class Attorney implements Person {
  String getFullName() {
    return 'Atty.';
  }
}