class Building {
  String? _name;  // use _ in a variable to make it private
  int floors;
  String address;

  Building(this._name, {    // named parameters with an underscore (private) should be a required positional parameter (placed outside the optional required parameters)
    required this.floors, 
    required this.address,
    }) {
      print('A building object has been created.');
  }

  // The get and set allows for indirect access to class fields
  String? get Name {
    print('The building\'s name has been retrieved.');
    return this._name;
  }

  void set Name(String? name) {
    this._name = name;
    print('The building\'s name has been changed to ${this._name}.');   // $name
  }

  Map<String, dynamic> getProperties() {
    return {
      'name': this._name,
      'floors': this.floors,
      'address': this.address
    };
  }
}