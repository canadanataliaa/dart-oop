void main() {
  Building building = new Building(
    name: 'Caswym', 
    floors: 8, 
    address: '134 Timog Avenue, Brgy. Sacred Heart, Quezon City, Metro Manila',
    rooms: ['Tech', 'Dev']
  );

  print(building);      // returns "Instance of 'Building'"
  print(building.name);
  print(building.floors);
  print(building.address);
}

class Building {
  String name;
  int floors;
  String address;
  List<String> rooms;

  Building({
    required this.name,     
    required this.floors, 
    required this.address,
    required this.rooms     // if rooms can be nullable, remove required and put ? on the fields datatype
    }) {
      print('A building object has been created.');
  }
}

// After addressing the nullable fields
// class Building {
//   String name;
//   int floors;
//   String address;
//   List<String> rooms;   // rooms = [];

//   Building(this.name, this.floors, this.address, this.rooms) {
//     print('A building object has been created.');
//   }
// }

/*
  Class declaration syntax:
  class ClassName {
    late <fields>         // late, value will be given later on
    <constructors>
    <methods>
    <getters-or-setters>
  }
 */