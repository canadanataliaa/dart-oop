// function ot get total
// function to get discount

void main () {
  print(getTotal());
  print(getTotal( discount: 20));
  print(getTotal( discount: 40));
  print(getTotal( discount: 60));
  print(getTotal( discount: 80));
}

String getTotal({ int discount = 0}) {
  List<num> prices = [45, 34.2, 176.9, 32.2];
  
  num total = 0;
  for (var price in prices) {
    total += price;
  }

  return computeDiscount(total, discount).toStringAsFixed(2);
}

num computeDiscount(num total, int? discount) {
  if (discount == null) {
    return total;
  } else {
    num discountAmount = total * discount / 100;
    return total - discountAmount;
  }
}