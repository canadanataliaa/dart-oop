void main () {
  List<num> prices = [45, 34.2, 176.9, 32.2];

  print(getTotal(prices));
  print(getDiscount(20)(getTotal(prices)));
  print(getDiscount(40)(getTotal(prices)));
  print(getDiscount(60)(getTotal(prices)));
  print(getDiscount(80)(getTotal(prices)));
}

num getTotal(List<num> prices) {
  num total = 0;
  prices.forEach((num price) => total += price);
  return total;
}

Function getDiscount(int percentage) {
  return (num price) {
    return percentage == 0 ? price : (price - (price * percentage / 100)).toStringAsFixed(2); 
  };
}