void main() {
  Function discountBy25 = getDiscount(25);  // output is the anonymous function from lines 11- 13, lexical is the getDiscount(25)
  Function discountBy50 = getDiscount(50);
  // The discountBy25 is then considered as a closure
  // The closure has access to variables in its lexical scope

  print(discountBy25(1400));
  print(discountBy50(1400));

  print(getDiscount(25)(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  // When the getDiscount is used and the function below is returned,
  // the value of 'percentage' parameter is retained - that is called lexical scope
  return (num amount) {   
    return amount * percentage / 100;
  }; 
}