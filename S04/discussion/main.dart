// The main() function is the entry point of Dart application.
void main() {
  // print(getCompanyName());
  // print(getYearEstablished());
  // print(hasOnlineClasses());
  // print(getCoordinates());
  // print(combineAddress('134 Timog Avenue', 'Brgy. Sacred Heart', 'Quezon City', 'Metro Manila'));
  // print(combineName('John', 'Smith'));
  // print(combineName('John', 'Smith', isLasNameFirst: true));
  // print(combineName('John', 'Smith', isLasNameFirst: false));

  // List<String> persons = ['John Doe', 'Jane Doe'];
  // List<String> students = ['Nicholas Rush', "James Holden"];

  // Anonymous functions - used if function is used once
  // persons.forEach((String person) => print(person));
  // persons.forEach((String person) {
  //    print(person);
  // });

  // students.forEach((String person) {
  //   print(person);
  // });

  // Functions as objects and used as an argument
  // printName(value) is function execution/call/invocation
  // printName is reference to the given function
  // persons.forEach(printName);
  // students.forEach(printName);
}

void printName(String name) {
  print(name);
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablished() {
  return 2017;
}

bool hasOnlineClasses() {
  return true;
}

// bool isUnderAge(int age) {
//   return age < 18;
// }

// The initial underAge function above can be changed into a lambda or arrow function
// A lambda function is a shortcut function for returning values from simple operations
// Lambda syntax: return-type function-name(parameters) => expression;
bool isUnderAge(int age) => age < 18;

Map<String, double> getCoordinates() {
  return {
    'latitude': 14.632702,
    'longitude': 121.043716
  };
}

String combineAddress(String specifics, String barangay, String city, String province) {
  return '$specifics $barangay $city $province';
}

// Optional named parameters - extending a feature
// To make this function display lastname or firstname fisrt
// These parameters are added after the required ones, added inside a {}
// If no value is given, default value can be assigned { bool isLasNameFirst = false }
String combineName(String firstName, String lastName, { bool isLasNameFirst = false }) {
  return isLasNameFirst ? '$lastName, $firstName' : '$firstName $lastName';
}

/* 
  Function Syntax:
  return-type function-name(param-data-type parameterA, param-data-type parameterB) {
    // code logic inside a function
    return 'return value';
  }

  Arguments are the values being passed to the function.
  Paramters are the values being received by the function.
  x (parameter) = 5 (argument)

  In Dart, writing the data type of a function paramter is not required
  The reason is related to code readability and conciseness
  dynamic is the default data type for parameters
 */