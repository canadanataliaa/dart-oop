// Create an s08/main.dart file and put the comments here to the Dart file.

// Create an abstract class named Equipment and an 
// abstract method named describe inside the abstract class.

// Create the classes Bulldozer, TowerCrane, and Loader 
// that will all implement the abstract class Equipment.

// Create objects for each of the vehicle type with the following information.
// - Bulldozer: Caterpillar D10, U blade
// - Tower Crane: 370 EC-B 12 Fibre, 78m hook radius, 12,000kg max capacity
// - Loader: Volvo L60H, wheel loader, 16530lbs tipping load

// Inside the main() method, create a List for the 
// equipment (using the Equipment type) and give it an initial value of [].

// Add the created objects to the list created earlier.

// Loop through each vehicle in the list and execute the describe() method.

// Sample Output
// - The bulldozer Caterpillar D10 has a U blade.
// - The tower crane 370 EC-B 12 Fibre has a radius of 78 and a max capacity of 12000.
// - The loader Volvo L60H is a wheel loader and has a tipping load of 16530 lbs.

void main() {
  List<Equipment> vehicles = [];

  Bulldozer bulldozer = new Bulldozer(
    name: 'Caterpillar D10', 
    blade: 'U blade'
  );
  TowerCrane towerCrane = new TowerCrane(
    name: '370 EC-B 12 Fibre', 
    radius: 78, 
    maxCapacity: 12000
  );
  Loader loader = new Loader(
    name: 'Volvo L60H', 
    type: 'wheel loader', 
    tippingLoad: 16530
  );

  vehicles.addAll([bulldozer, towerCrane, loader]);
  vehicles.forEach((Equipment vehicle) => {
    print(vehicle.describe())
  });
}

abstract class Equipment {
  String describe();
}

class Bulldozer implements Equipment {
  String name;
  String blade;

  Bulldozer({
    required this.name,
    required this.blade
  });

  String describe() {
    return 'The bulldozer ${this.name} has a ${this.blade}.';
  }
}

class TowerCrane implements Equipment {
  String name;
  num radius;
  num maxCapacity;

  TowerCrane({
    required this.name,
    required this.radius,
    required this.maxCapacity
  });

  String describe() {
    return 'The tower crane ${this.name} has a radius of ${this.radius} and a max capacity of ${this.maxCapacity}.';
  }
}

class Loader implements Equipment {
  String name;
  String type;
  num tippingLoad;

  Loader({
    required this.name,
    required this.type,
    required this.tippingLoad
  });

  String describe() {
    return 'The loader ${this.name} is a ${this.type} and has a tipping load of ${this.tippingLoad} lbs.';
  }
}