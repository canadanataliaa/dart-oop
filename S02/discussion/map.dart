void main() {
  // Map is used to represent an object
  // const Map<keyDatatype, ValueDatatype> variableName = {};
  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'disctrict': 'Marylebone',
    'city': 'London',
    'country': 'United Kingdom'
  };

  address['region'] = 'Europe';

  print(address);
}