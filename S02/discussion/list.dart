void main() {
  // List can be ordered/unordered and have duplicate values, can be accessed using numeric index
  // List<dataType> variableName = [values];
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ['John', 'Jane', 'Tom'];

  // const List<num> = const [];
  const List<String> maritalStatus = [
    'Single',
    'Married',
    'Divorced',
    'Widowed'
  ];

  print(discountRanges);
  print(names);

  print(discountRanges[2]);
  print(names[0]);

  print(discountRanges.length);
  print(names.length);

  names[0] = 'Jonathan';

  names.add('Mark'); // add element to the end of the list
  names.insert(0,'Roselle'); // add element to the beginning or any index you prefer of a list
  print(names);

  // List properties
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

  // List methods
  // sort() method modifies the list itself
  names.sort();
  // names.sort((a, b) => a.length.compareTo(b.length));
  print(names.reversed);
}
