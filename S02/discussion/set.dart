void main() {
  // Sets in Dart are an unordered collection of unique items, cannot be accessed using numeric index
  // const Set<dataType> variableName = {values};
  Set<String> subcontractors = {'Sonderhoff', 'Stahlscmidt'};

  subcontractors.add('Schweisstechnik');
  subcontractors.add('Kreatel');
  subcontractors.add('Kunstoffe');

  subcontractors.remove('Sonderhoff');

  print(subcontractors);
}
