void main() {
  List<String> processors = ['Ryzen 5 3600', 'Ryzen 3 3200G', 'Ryzen 9 5950X', 'Ryzen 7 5800X'];
  Set<String> countries = {'United Kingdom', 'Thailand', 'Philippines', 'Canada', 'Singapore', 'Japan'};
  Map<String, dynamic> person = {
    'FirstName': 'John',
    'LastName': 'Smith',
    'Age': 34
  };

  print(processors.length);
  print(countries.length);
  print(person.length);
}