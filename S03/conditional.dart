void main () {
  print(showServiceItems('robotics'));
  print(determinTyphoonIntensity(67));
  print(selectSector(3));
  print(isUnderAge(19));
}

List<String> showServiceItems(String category) {
  if (category == 'apps') {
    return ['native', 'android', 'ios', 'web'];
  } else if (category == 'cloud') {
    return ['azure', 'microservices'];
  } else if (category == 'robotics') {
    return ['sensors', 'fleet-tracking', 'realtime-communication'];
  } else {
    return [];
  }

}

String determinTyphoonIntensity(int windSpeed) {
  if (windSpeed < 30) {
    return 'Not a typhoon yet.';
  } else if (windSpeed <= 61) {
    return 'Tropical depression detected.';
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return 'Tropical storm detected.';
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return 'Severe tropical storm detected.';
  } else {
    return 'Typhoon detected.';
  }
}

String selectSector(int sectorID) {
  String name;

  switch(sectorID) {
    case 1: 
      name = 'Craft';
      break;
    case 2:
      name = 'Assembly';
      break;
    case 3:
      name = 'Building Operations';
      break;
    default:
      name = sectorID.toString() + ' is out of bounds';
      // break; optional
  }

  return name;
}

bool isUnderAge(age) {
  // return age < 18 ? true : false;   // Ternary condition
  return age < 18;
}

