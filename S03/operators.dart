void main() {
  // Assignment operator =
  int x = 1397;
  int y = 7831;

  // Arithmetic operators +-*/%
  num sum = x + y;
  num difference = x - y;
  num product = x * y;
  num quotient = x / y;
  num remainder = x % y;
  num output = (x * y) - (x / y + x);

  // Relational operators
  bool isGreaterThan = x > y;   // x > y is the expression, the whole line is the statement
  bool isLessThan = x < y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  // Logical operators
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isEqual && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered;
  
  // Increment & decrement operators
  print(x++);
  print(y--);
}