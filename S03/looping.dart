void main() {
  // whileLoop();
  // doWhileLoop();
  // forLoop();
  // forInLoop();
  modifiedForLoop();
}

// Check the condition first before execute
void whileLoop() {
  int count = 5;

  while (count != 0) {
    print(count);
    count--;
  }
}

// Execute first then check condition
void doWhileLoop() {
  int count = 20;

  do {
    print(count);
    count--;
  } while (count > 0);
}

// for(initialization; condition; iterator)
void forLoop() {
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

// for-in loops does not require declaration of initial value
void forInLoop() {
  List<Map<String, String>> persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'},
  ];

  for (var person in persons) {
    print(person);  // person['name']
  }
}

// branching: "continue" keyword
void modifiedForLoop() {
  for(int count = 0; count <= 20; count++) {
    if (count > 10) {       // excludes 11
      break;
    }
    if (count % 2 == 0) {
      continue;
    } else {
      print(count);
    }
    // if (count > 10) {    // includes 11
    //   break;
    // }
  }
}