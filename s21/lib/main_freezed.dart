import './freezed_models/user.dart';
import './freezed_models/task.dart';
import './freezed_models/note.dart';

void main() {
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    Task taskA = Task(id: 1, userId: 1, description: 'task description', isDone: 1);
    Note noteA = Note(id: 1, userId: 1, title: 'note title', description: 'note description');

    // taskA.description = 'new description';
    // noteA.title = 'new title';

    var taskResponse = {'id': 1, 'userId': 1, 'description': 'task description', 'isDone': 1};
    var noteResponse = {'id': 1, 'userId': 1, 'title': 'task title', 'description': 'note description'};

    Task taskB = Task.fromJson(taskResponse);
    print(taskB);
    print(taskA.toJson());

    Note noteB = Note.fromJson(noteResponse);
    print(noteB);
    print(noteA.toJson());

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);

    // Demonstration of object immutability below.
    // Immutability means changes are not allowed.
    // Mutable (can be changed), immutable (cannot be changed).By default, objects you create should be immutable.
    // It ensures that an object will not be changed accidentally.
    // Freezed package takes care of this object immutability.

    // Instead of directly changing the object's property, the object itself will be changed.
    // Use userA = userA.copyWith(email: 'john@hotmail.com');   copyWith = copying with changes

    // print(userA.email);
    // // userA.email = 'john@hotmail.com';
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    // var response = {'id': 3, 'email': 'doe@gmail.com'};

    // User userC = User(
    //     id: response['id'] as int,
    //     email: response['email'] as String
    // );

    // User userC = User.fromJson(response);
    // print(userC);
    // print(userC.toJson());
}
