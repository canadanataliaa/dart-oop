import 'package:flutter/material.dart';
import 'answer_button.dart';

class BodyQuestions extends StatelessWidget {
    final questions;
    final int questionIdx;
    final Function nextQuestion;      // use Function() when receiving a function from outside and if you dont have a parameter

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });
    
    @override 
    Widget build(BuildContext context) {
        Text questionText = Text(
            questions[questionIdx]['question'].toString(),
            style: TextStyle(
                fontSize: 18.0
            )
        );

        var options = questions[questionIdx]['options'] as List<String>;
        var answerOptions = options.map((String option) {
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });

        return Container(
            width: double.infinity,
            padding: EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    questionText,
                    // for ( var ans in answers ) 
                    //   if (answers.indexOf(ans) == questionIdx)
                    //     for (var choice in ans) 
                    //       AnswerButton(text: choice, nextQuestion: nextQuestion)
                    // ,
                    // createRadioTile(),
                    ...answerOptions,
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        width: double.infinity,
                        child: ElevatedButton(
                            child: Text('Skip question'),
                            onPressed: () => nextQuestion(null),
                        ),
                    ),
                ],
            ),
         );
    }
}