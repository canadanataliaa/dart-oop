import 'package:flutter/material.dart';
import 'body_questions.dart';
import 'body_answers.dart';

void main() {
    runApp(App());  // The App() is the root widget.
}

class App extends StatefulWidget {
    @override
    AppState createState() => AppState();
}

class AppState extends State<App> {
    int questionIdx = 0;
    bool showAnswers = false;

    final questions = [
        {
            'question': 'What is the nature of your business needs?',
            'options': ['Time Tracking', 'Asset Management', 'Issue Tracking']
        },
        {
            'question': 'What is the expected size of the user base?',
            'options': ['Less than 1,000', 'Less than 10,000', 'More than 10,000']
        },
        {
            'question': 'In which region would the majority of the user base be?',
            'options': ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East']
        },
        {
            'question': 'What is the expected project duration?',
            'options': ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More than 12 months']
        }
    ];
  
    var answers = [];
    
    void nextQuestion(String? answer) {
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer': answer == null ? '' : answer
        });

        questionIdx < questions.length-1 
            ? setState(() => questionIdx++) 
            : setState(() => showAnswers = true);
    }

    @override
    Widget build(BuildContext context) {
        var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
        var bodyAnswers = BodyAnswers(answers: answers);

        Scaffold homepage = Scaffold(
            appBar: AppBar(
                title: Text('Homepage')),
            body: showAnswers ? bodyAnswers : bodyQuestions
        );

        return MaterialApp(
            home: homepage,
        );
    }
}

// The invisible widgets are the Container and Scaffold.
// The visible widgets are AppBar and Text.
// A Container can only have one child.

// The Scaffold widget provides basic design layout structure.
// The Scaffold can be given UI elements such as an app bar.

// To specify margin or padding spacing, we can use EdgeInsets
// The values for spacing are in multiples of 8 (e.g 16, 24, 32)
// To add spacing on all sides, use EdgeInsets.all()
// To add spacing on certain sides, use EdgeInsets.only(direction: value)

// In Flutter, width = double.infinity is the equivalent of width = 100% in CSS.

// In a Column widget, MainAxisAlignment is vertical (top to bottom)
// To change horizontal alignment of widgets in a column, use CrossAxisAlignment
// In Columns, main is vertical and cross is horizontal

// Typecasting:
//  AnswerButton(text: (questions[questionIdx]['options'] as List<String>)[0], nextQuestion: nextQuestion),

// Widget createRadioTile() {
  //   for ( var ans in questions ) {
  //     if (questions.indexOf(ans) == questionIdx) 
  //       for (var choice in ans['options'] as List<String>)
  //         return AnswerButton(text: choice, nextQuestion: nextQuestion);
  //   }
  // }

// Spread opertator
// [questionText, [answerOptions], skipQuestion]
// [questionText, ...answerOptions, skipQuestion]
// [questionText, answerButtonA, answerButtonB, answerButtonN, skipQuestion]

// State is lifted up
// App.questionIdx = 2
// -> Bodyquestions.questionIdx = 2