void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(lastName: 'Westwood', firstName: 'Jonas', employeeID: 'ID-001');

  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
  String firstName;
  String lastName;

  Person({
    required this.firstName,
    required this.lastName
  });

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  String employeeID;

  Employee({ required String firstName, required String lastName, required this.employeeID }) :   // firstName, lastName are inherited from Person
    super(firstName: firstName, lastName: lastName) {
  }
}

// simpler method (no constructor)

// void main() {
//   Person person = new Person();
//   Employee employee = new Employee();

//   person.firstName = 'John';
//   person.lastName = 'Smith';

//   employee.firstName = 'Jonas';
//   employee.lastName = 'Westwood';

//   print(person.getFullName());
//   print(employee.getFullName());
// }

// class Person {
//   String? firstName;
//   String? lastName;

//   String getFullName() {
//     return '${this.firstName} ${this.lastName}';
//   }
// }

// class Employee extends Person {

// }